# Morse Code Translator

![https://docs.python.org/3/](https://img.shields.io/badge/Python-3.8%20%7C%203.9%20%7C%203.10-green?style=plastic&logo=python) ![Terminal](https://img.shields.io/badge/Terminal%20Interface-blue)

## Descripción

Esta utilidad fue desarollada por @gustavo-arboleda con el propósito de entretener y llevarnos a aquella época en la que las comunicaciones a larga distancia se daba utilizando el ingenioso y fascinante código morse. Este proyesto fué pensado para hacer que el usuario pueda interactuar con el programa como se interactuaba con el telégrafo en determinado tiempo de nuestra historia de nuestra humanidad.

Este traductor de código morse nos permite convertir de alphanumérico a código morse, con la posibilidad de escuchar los sonidos del código que se está traduciendo. así mismo, se puede traducir de código morse a alfanumérico, trauciendo caracter a caracter en tiempo real y mostrando un resultado de la frase traducida al finalizar la traducción.

## Descárgalo en:

- Clona el repositorio por HTTPS usando:
  https://gitlab.com/gustavo-arboleda/morse-code.git

- Descarga el código fuente desde la página de relases:
  https://gitlab.com/gustavo-arboleda/morse-code/-/releases

## ¿Cómo funciona?

Este traductor tiene un menú donde muestra las opciones de las cuales el usuario puede escoger, las opciones son las que se ven en la imágen a continuación:

![Menu](/images/menu.png)

## Guía de instalación

### Requerimientos

- Python 3.8 o superior
- Git
- PIP
- Se recomienda usar para la terminal la fuente MesloLGS NF Bold o alguna fuente que tenga soporte para eimojs.

### Pasos para la instalación

1. Clonar el repositorio en el directorio de preferencia ejecutando el comando:
   `git clone https://gitlab.com/gustavo-arboleda/morse-code.git`

2. Posicionarse en el directorio del proyecto: `cd morse-code`

3. Crear un entorno virtual:
   Para Ubuntu: `python3 -m venv environment`

4. Activar el entorno virtual:
   Para Ubuntu: `source environment/bin/activate`

5. Instalar las dependencias del proyecto: `pip install -r requirements.txt`

6. Ejecutar el menú principal: `python3 src/main.py`

7. Disfrutar del traductor!! 🤓🎉

## Imágenes de la interfaz

### Alfanumérico a morse

![Traducción de alfanumérico a morse](/images/alpha_to_morse.png)

### Morse a alfanumérico

![Traducción de morse a alfanumérico](/images/morse_to_alpha.png)

### Configuraciones

![Configuraciones](/images/settings.png)

### Acerca de

![About](/images/about.png)
