class AlphaGenerator:
    _symbols = {
        "-----": "0",
        ".----": "1",
        "..---": "2",
        "...--": "3",
        "....-": "4",
        ".....": "5",
        "-....": "6",
        "--...": "7",
        "---..": "8",
        "----.": "9",
        ".-": "a",
        "-...": "b",
        "-.-.": "c",
        "-..": "d",
        ".": "e",
        "..-.": "f",
        "--.": "g",
        "....": "h",
        "..": "i",
        ".---": "j",
        "-.-": "k",
        ".-..": "l",
        "--": "m",
        "-.": "n",
        "---": "o",
        ".--.": "p",
        "--.-": "q",
        ".-.": "r",
        "...": "s",
        "-": "t",
        "..-": "u",
        "...-": "v",
        ".--": "w",
        "-..-": "x",
        "-.--": "y",
        "--..": "z",
        ".-.-.-": ".",
        "--..--": ",",
        "..--..": "?",
        "-.-.--": "!",
        "-....-": "-",
        "-..-.": "/",
        ".--.-.": "@",
        "-.--.": "(",
        "-.--.-": ")",
        " ": " ",
    }
    alpha_expression = ""
    alpha_phrase = ""

    def __init__(self):
        pass

    def run(self, character: str):
        if character in self._symbols:
            self.alpha_expression = self._symbols[character]
            self.alpha_phrase += self._symbols[character]
            print(self.alpha_expression, "\n")

    def get_alpha_phrase(self):
        return self.alpha_phrase

    def set_alpha_phrase(self, phrase: str):
        self.alpha_phrase = phrase

    def set_alpha_expression(self, expression: str):
        self.alpha_expression = expression
