from time import sleep
from pygame import mixer


class MorseGenerator:
    _symbols = {
        "0": "-----",
        "1": ".----",
        "2": "..---",
        "3": "...--",
        "4": "....-",
        "5": ".....",
        "6": "-....",
        "7": "--...",
        "8": "---..",
        "9": "----.",
        "a": ".-",
        "b": "-...",
        "c": "-.-.",
        "d": "-..",
        "e": ".",
        "f": "..-.",
        "g": "--.",
        "h": "....",
        "i": "..",
        "j": ".---",
        "k": "-.-",
        "l": ".-..",
        "m": "--",
        "n": "-.",
        "o": "---",
        "p": ".--.",
        "q": "--.-",
        "r": ".-.",
        "s": "...",
        "t": "-",
        "u": "..-",
        "v": "...-",
        "w": ".--",
        "x": "-..-",
        "y": "-.--",
        "z": "--..",
        ".": ".-.-.-",
        ",": "--..--",
        "?": "..--..",
        "!": "-.-.--",
        "-": "-....-",
        "/": "-..-.",
        "@": ".--.-.",
        "(": "-.--.",
        ")": "-.--.-",
        " ": " ",
    }
    _space_char = " "
    _dot_char = "."
    _dash_char = "-"
    _sound_on = True
    phrase = ""
    morse_expression = ""

    def __init__(self, phrase: str = ""):
        self.phrase = phrase

    def run(self):
        self.initialize()

        morse_prase = ""
        for letter in self.phrase.lower():
            morse_key = self._symbols.get(letter)
            if morse_key is None:
                morse_key = " "
            morse_prase += morse_key
            morse_prase += "|"

        for char_ in morse_prase:
            if char_ == ".":
                self.dot()
            elif char_ == "-":
                self.dash()
            elif char_ == "|":
                self._between_letters()
            elif char_ == " ":
                self.space()

        self.shut_down()

    def dash(self):
        self._write_dash()
        self._play_dash()
        self._between_letter_parts()

    def dot(self):
        self._write_dot()
        self._play_dot()
        self._between_letter_parts()

    def space(self):
        self._write_space()
        self._play_space()

    def _play_dash(self):
        if not self._sound_on:
            return
        beep = mixer.Sound("./sounds/long-beep.wav")
        beep.play()
        sleep(0.9)
        beep.stop()

    def _write_dash(self):
        self.morse_expression += self._dash_char
        print(self._dash_char)

    def _play_dot(self):
        if not self._sound_on:
            return
        beep = mixer.Sound("./sounds/long-beep.wav")
        beep.play()
        sleep(0.3)
        beep.stop()

    def _write_dot(self):
        self.morse_expression += self._dot_char
        print(self._dot_char)

    def _play_space(self):
        if not self._sound_on:
            return
        sleep(2.1)

    def _write_space(self):
        self.morse_expression += self._space_char
        print(self._space_char)

    def _between_letters(self):
        sleep(0.9)

    def _between_letter_parts(self):
        sleep(0.3)

    def initialize(self):
        mixer.init()
        mixer.music.set_volume(0.1)

    def shut_down(self):
        self.morse_expression.replace("  ", " ")

    def set_input(self, phrase: str):
        self.phrase = phrase

    def set_sound_on(self, sound_on: bool):
        self._sound_on = sound_on

    def set_morse_expression(self, morse_expression: str):
        self.morse_expression = morse_expression
