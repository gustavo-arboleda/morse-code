import os
from morse_generator import MorseGenerator
from alpha_generator import AlphaGenerator


class MainMenu:
    morse_traslator: MorseGenerator
    alpha_gen = AlphaGenerator()

    def __init__(self):
        self.morse_traslator = MorseGenerator()

    def main_menu(self):
        """
        Menú principal
        """
        menu_msg = """

            **      **  *********  *********  *********  *********
            ** *  * **  **     **  **     **  **         **
            **  *   **  **     **  ** ******  *********  *******
            **      **  **     **  **    *           **  **
            **      **  *********  **     **  *********  *********

                **      **  *********  **     **  **      **
                ** *  * **  **         ** *   **  **      **
                **  *   **  *******    **  *  **  **      **
                **      **  **         **   * **  **      **
                **      **  *********  **    ***  *********
        """
        options = """
            1. From alphanumeric to morse

            2. From morse to alphanumeric

            3. Settings

            4. About

            0. Exit
        """

        os.system("clear")
        print(menu_msg)
        print(options)

        try:
            user_input = int(input(""))
        except ValueError:
            self.main_menu()
        except KeyboardInterrupt:
            print("\nCome back soon! 🥳")
            exit()

        if user_input == 1:
            os.system("clear")
            _input = self.read_input("Input the text for translating it to morse: ")
            # Convierte a código morse
            self.morse_traslator.set_input(_input)
            self.morse_traslator.set_morse_expression("")
            print("Translating to morse...")
            self.morse_traslator.run()

            # Retorna lo ejecutado
            print("\nTaken phrase: ")
            print(_input)
            print("\nMorse expression: ")
            print(self.morse_traslator.morse_expression)
            print("\nPress return to continue...")
            try:
                _input = input("")
                raise KeyboardInterrupt()
            except KeyboardInterrupt:
                self.main_menu()

        elif user_input == 2:
            os.system("clear")
            _input = self.read_input(
                "Insert the the morse code for a letter, then press return:\n"
            )
            # Reinicia los valores de la clase
            self.alpha_gen.set_alpha_phrase("")
            self.alpha_gen.set_alpha_expression("")

            self.convert_morse_to_alpha(_input)
        elif user_input == 3:
            self.settings_menu()
        elif user_input == 4:
            self.about()
            self.main_menu()
        elif user_input == 0:
            print("\nCome back soon! 🥳")
            exit()

    def read_input(self, msg: str) -> str:
        try:
            user_input = input(msg)
            return user_input
        except KeyboardInterrupt:
            self.main_menu()

    def settings_menu(self, message: str = None):
        """Menú de configuración del juego"""
        options = """
            1. Turn sound on

            2. Turn sound off

            0. Main menu
        """

        os.system("clear")
        print(options)
        if message is not None:
            print(message)

        try:
            user_input = int(input(""))
        except ValueError:
            self.settings_menu()
        except KeyboardInterrupt:
            self.main_menu()

        if user_input == 1:
            self.morse_traslator.set_sound_on(True)
            self.settings_menu("Now sound is on...")
        elif user_input == 2:
            self.morse_traslator.set_sound_on(False)
            self.settings_menu("Now sound is off...")
        elif user_input == 0:
            self.main_menu()

    def convert_morse_to_alpha(self, input_: str):
        try:
            abort_ = False

            if input_ == "":
                abort_ = True

            while not abort_:
                try:
                    # Convierte a código morse a texto alfanumérico
                    self.alpha_gen.run(input_)
                    input_ = input()
                    self.convert_morse_to_alpha(input_)
                except KeyboardInterrupt:
                    abort_ = True

            print("\nEntire word:", self.alpha_gen.get_alpha_phrase())
            input_ = input()
            self.main_menu()
        except KeyboardInterrupt:
            self.main_menu()

    def about(self):
        """Muestra mensaje de 'acerca de' del Juego"""
        about_msg = """
                **      **  *********  ********    *********
                ** *  * **  **     **  **      **  **
                **  *   **  **     **  **      **  *******
                **      **  *********  **      **  **
                **      **  **     **  ********    *********

                **      **  ********  ********  **      **
                **      **     **        **     **      **
                **  *   **     **        **     **********
                ** *  * **     **        **     **      **
                **      **  ********     **     **      **
        """
        heart_msg = """
                    00000000000           000000000000
                  00000000     00000   000000     0000000
                0000000             000              00000
               0000000               0                 0000
              000000                                    0000
              00000                                      0000
             00000                                      00000
             00000                                     000000
              000000                                 0000000
               0000000                              0000000
                 000000                            000000
                   000000                        000000
                      00000                     0000
                         0000                 0000
                           0000             000
                             000         000
                                000     00
                                  00  00
                                    00
        """
        os.system("clear")
        print(about_msg)
        print(heart_msg)
        try:
            print("By: Gustavo Arboleda 💙🤓⌨️")
        except ValueError:
            print("By: Gustavo Arboleda...")
        print("Email: gustavoandres.arboleda@gmail.com")
        print("\n")
        print("Press return to continue...")
        try:
            input("")
        except KeyboardInterrupt:
            self.main_menu()


if __name__ == "__main__":
    menu_instance = MainMenu()
    menu_instance.main_menu()
